import React, { useEffect, useState } from 'react';
// import memesData from '../memesData';

export default function Meme() {
  const [memeImage, setMemeImage] = useState({
    topText: '',
    bottomText: '',
    randomImage: 'http://i.imgflip.com/1bij.jpg',
  });

  const [allMemeImages, setAllMemeImages] = useState([]);

  useEffect(() => {
    async function getMemes() {
      const res = await fetch("https://api.imgflip.com/get_memes");
      const data = await res.json();
      setAllMemeImages(data.data.memes);
    }

    getMemes()
    // .then(res => res.json()).then(data => setAllMemeImages(data.data.memes));
  }, []);


  function getMemeImage() {
    // const memesArray = allMemeImages.data.memes;
    const randomNumber = Math.floor(Math.random() * allMemeImages.length);
    const { url } = allMemeImages[randomNumber];
    // console.log(url);
    setMemeImage((prevMeme) => ({ ...prevMeme, randomImage: url }));
  }

  function handleChange(event) {
    const { name, value } = event.target;
    setMemeImage(prevMeme => ({ ...prevMeme, [name]: value }))

  }

  return (
    <main>
      <div className="form">
        <input
          name="topText"
          value={memeImage.topText}
          onChange={handleChange}
          type="text"
          placeholder="Top text"
          className="form-input"
        />
        <input
          name="bottomText"
          value={memeImage.bottomText}
          onChange={handleChange}
          type="text"
          placeholder="Bottom text"
          className="form-input"
        />
        <button className="form-button" onClick={getMemeImage}>
          Get a new meme image
        </button>
      </div>
      <div className="meme">
        <img src={memeImage.randomImage} className="meme-img" />
        <h2 className="meme--text top">{memeImage.topText}</h2>
        <h2 className="meme--text bottom">{memeImage.bottomText}</h2>
      </div>
    </main>
  );
}

